require "Cocos2d"
require "Cocos2dConstants"

local TitleScene = class("TitleScene",function()
    return cc.Scene:create()
end)

function TitleScene.create()
    local scene = TitleScene.new()
    scene:addChild(scene:createLayerFarm())
    scene:addChild(scene:createLayerMenu())
    return scene
end

function TitleScene:ctor()
    self.visibleSize = cc.Director:getInstance():getVisibleSize()
    self.origin = cc.Director:getInstance():getVisibleOrigin()
    self.schedulerID = nil
end

function TitleScene:playBgMusic()
    local bgMusicPath = cc.FileUtils:getInstance():fullPathForFilename("background.mp3") 
    cc.SimpleAudioEngine:getInstance():playMusic(bgMusicPath, true)
    local effectPath = cc.FileUtils:getInstance():fullPathForFilename("effect1.wav")
    cc.SimpleAudioEngine:getInstance():preloadEffect(effectPath)
end

-- create farm
function TitleScene:createLayerFarm()
    local layerFarm = cc.Layer:create()
    -- add in farm background
    local bg = cc.Sprite:create("farm.jpg")
    bg:setPosition(self.origin.x + self.visibleSize.width / 2 + 80, self.origin.y + self.visibleSize.height / 2)
    layerFarm:addChild(bg)

    -- handing touch events
    local touchBeginPoint = nil
    local function onTouchBegan(touch, event)
        local location = touch:getLocation()
        -- cclog("onTouchBegan: %0.2f, %0.2f", location.x, location.y)
        touchBeginPoint = {x = location.x, y = location.y}
        -- CCTOUCHBEGAN event must return true
        return true
    end

    local function onTouchMoved(touch, event)
        local location = touch:getLocation()
        --cclog("onTouchMoved: %0.2f, %0.2f", location.x, location.y)
        if touchBeginPoint then
            local cx, cy = layerFarm:getPosition()
            layerFarm:setPosition(cx + location.x - touchBeginPoint.x,
                cy + location.y - touchBeginPoint.y)
            touchBeginPoint = {x = location.x, y = location.y}
        end
    end

    local function onTouchEnded(touch, event)
        local location = touch:getLocation()
        --cclog("onTouchEnded: %0.2f, %0.2f", location.x, location.y)
        touchBeginPoint = nil
        spriteDog.isPaused = false
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    local eventDispatcher = layerFarm:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, layerFarm)

    local function onNodeEvent(event)
        if "exit" == event then
            cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.schedulerID)
        end
    end
    layerFarm:registerScriptHandler(onNodeEvent)

    return layerFarm
end

-- create menu
function TitleScene:createLayerMenu()

    local layerMenu = cc.Layer:create()
    local menuPopup, menuTools, effectID

    local function menuCallbackClosePopup()
        -- stop test sound effect
        cc.SimpleAudioEngine:getInstance():stopEffect(effectID)
        menuPopup:setVisible(false)
    end

    local function menuCallbackOpenPopup()
        -- loop test sound effect
        --local effectPath = cc.FileUtils:getInstance():fullPathForFilename("effect1.wav")
        --effectID = cc.SimpleAudioEngine:getInstance():playEffect(effectPath)
        --menuPopup:setVisible(true)
        
        -- ゲームへ移動
        --create scene
        local scene = require("GameScene")
        local gameScene = scene.create()
        gameScene:playBgMusic()

        if cc.Director:getInstance():getRunningScene() then
            cc.Director:getInstance():replaceScene(gameScene)
        else
            cc.Director:getInstance():runWithScene(gameScene)
        end 
    end

    -- add a popup menu
    -- メニュー
    local menuPopupItem = cc.MenuItemImage:create("menu2.png", "menu2.png")
    menuPopupItem:setPosition(0, 0)
    menuPopupItem:registerScriptTapHandler(menuCallbackClosePopup)
    menuPopup = cc.Menu:create(menuPopupItem)
    menuPopup:setPosition(self.origin.x + self.visibleSize.width / 2, self.origin.y + self.visibleSize.height / 2)
    menuPopup:setVisible(false)
    layerMenu:addChild(menuPopup)

    -- add the left-bottom "tools" menu to invoke menuPopup
    -- 除草
    local menuToolsItem = cc.MenuItemImage:create("menu1.png", "menu1.png")
    menuToolsItem:setPosition(0, 0)
    menuToolsItem:registerScriptTapHandler(menuCallbackOpenPopup)
    menuTools = cc.Menu:create(menuToolsItem)
    local itemWidth = menuToolsItem:getContentSize().width
    local itemHeight = menuToolsItem:getContentSize().height
    menuTools:setPosition(self.origin.x + itemWidth/2, self.origin.y + itemHeight/2)
    layerMenu:addChild(menuTools)

    return layerMenu
end

return TitleScene
