require("Cocos2d")
local cclog
cclog = function(...)
  return print(string.format(...))
end
__G__TRACKBACK__ = function(msg)
  cclog("----------------------------------------")
  cclog("LUA ERROR: " .. tostring(msg) .. "\n")
  cclog(debug.traceback())
  cclog("----------------------------------------")
  return msg
end
local main
main = function()
  collectgarbage("collect")
  collectgarbage("setpause", 100)
  collectgarbage("setstepmul", 5000)
  cc.FileUtils:getInstance():addSearchPath("src")
  cc.FileUtils:getInstance():addSearchPath("res")
  cc.Director:getInstance():getOpenGLView():setDesignResolutionSize(480, 320, 0)
  local scene = require("TitleScene")
  local titleScene = scene.create()
  if cc.Director:getInstance():getRunningScene() then
    return cc.Director:getInstance():replaceScene(titleScene)
  else
    return cc.Director:getInstance():runWithScene(titleScene)
  end
end
local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
  return error(msg)
end
