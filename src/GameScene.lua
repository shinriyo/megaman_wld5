require "Cocos2d"
require "Cocos2dConstants"

local GameScene = class("GameScene",function()
    return cc.Scene:create()
end)

function GameScene.create()
    local scene = GameScene.new()
    scene:addChild(scene:createMap())
    scene:addChild(scene:createLayerBackground())
    scene:addChild(scene:createLayerMenu())
    return scene
end

function GameScene:ctor()
    self.visibleSize = cc.Director:getInstance():getVisibleSize()
    self.origin = cc.Director:getInstance():getVisibleOrigin()
    self.schedulerID = nil
end

function GameScene:playBgMusic()
    local bgMusicPath = cc.FileUtils:getInstance():fullPathForFilename("background.mp3") 
    cc.SimpleAudioEngine:getInstance():playMusic(bgMusicPath, true)
    local effectPath = cc.FileUtils:getInstance():fullPathForFilename("effect1.wav")
    cc.SimpleAudioEngine:getInstance():preloadEffect(effectPath)
end

-- マップの読み込み
function GameScene:createMap()
    local map = cc.TMXTiledMap:create("plute/plute_stage.tmx")
    --gameScene:addChild(map, -1, MapTag)
    return map
end

local isPushLeft = false
local isPushRight = false
local isPushBuster = false
local moveDown = false

function GameScene:createMegaman()
    -- 1コマの幅
    local frameWidth = 32
    local frameHeight = 32

    -- create megaman animate
    local textureMegaman = cc.Director:getInstance():getTextureCache():addImage("res/megaman/megaman.png")
    -- appear
    local rect = cc.rect(frameWidth*0, frameHeight*0, frameWidth, frameHeight)
    local appearFrame0 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*1, frameHeight*0, frameWidth, frameHeight)
    local appearFrame1 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*2, frameHeight*0, frameWidth, frameHeight)
    local appearFrame2 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    local appearAnimation = cc.Animation:createWithSpriteFrames({appearFrame0,appearFrame1}, 0.5)

    -- sliding
    --[[
    rect = cc.rect(frameWidth*0, frameHeight*1, frameWidth, frameHeight)
    local standFrame0 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*1, frameHeight*1, frameWidth, frameHeight)
    local standFrame1 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*2, frameHeight*1, frameWidth, frameHeight)
    local standFrame2 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    local standAnimation = cc.Animation:createWithSpriteFrames(
        {standFrame0,standFrame1,standFrame2}, 0.2
    )
    ]]--
    
    -- stand
    rect = cc.rect(frameWidth*0, frameHeight*1, frameWidth, frameHeight)
    local standFrame0 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*1, frameHeight*1, frameWidth, frameHeight)
    local standFrame1 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*2, frameHeight*1, frameWidth, frameHeight)
    local standFrame2 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    local standAnimation = cc.Animation:createWithSpriteFrames(
        {standFrame0,standFrame1,standFrame2}, 0.2
    )
    local standAnimate = cc.Animate:create(standAnimation)
    --spriteMegaman:runAction(cc.RepeatForever:create(standAnimate))
    -- walk
    rect = cc.rect(frameWidth*3, frameHeight*1, frameWidth, frameHeight)
    local walkFrame0 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*4, frameHeight*1, frameWidth, frameHeight)
    local walkFrame1 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*5, frameHeight*1, frameWidth, frameHeight)
    local walkFrame2 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    local walkAnimation = cc.Animation:createWithSpriteFrames(
        {walkFrame0,walkFrame1,walkFrame2}, 0.2
    )
    
    -- buster
    rect = cc.rect(frameWidth*3, frameHeight*1, frameWidth, frameHeight)
    local busterFrame0 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    rect = cc.rect(frameWidth*4, frameHeight*1, frameWidth, frameHeight)
    local busterFrame1 = cc.SpriteFrame:createWithTexture(textureMegaman, rect)
    local busterAnimation = cc.Animation:createWithSpriteFrames(
        {busterFrame0,busterFrame1}, 0.2
    )
    
    -- 最初のコマ
    local spriteMegaman = cc.Sprite:createWithSpriteFrame(walkFrame0)
    spriteMegaman:setPosition(self.origin.x, self.origin.y + self.visibleSize.height / 4 * 3)
    --spriteMegaman.isPaused = false

    local walkAnimation = cc.Animation:createWithSpriteFrames(
        {walkFrame0,walkFrame1,walkFrame2,walkFrame1}, 0.2
    )

    local animate = cc.Animate:create(walkAnimation);
    spriteMegaman:runAction(cc.RepeatForever:create(animate))

    -- moving megaman at every frame
    -- 自動で動く
    local function tick()
        local x, y = spriteMegaman:getPosition()
        -- 内容
        local speed = 1.4
        local moveX = 0
        if isPushLeft then
            moveX = -1
        end
        if isPushRight then
            moveX = 1
        end
        --[[
        TODO: why can't?
        local moveX = isPushLeft and -1 or 0
        moveX = isPushRight and 1 or 0
        --]]
        spriteMegaman:setPositionX(x+moveX * speed)
    end

    self.schedulerID = cc.Director:getInstance():getScheduler():scheduleScriptFunc(tick, 0, false)

    return spriteMegaman
end

-- create farm
function GameScene:createLayerBackground()
    local layerBackground = cc.Layer:create()

    -- add moving megaman
    local spriteMegaman = self:createMegaman()
    layerBackground:addChild(spriteMegaman)

    -- Key
    local function onKeyPressed(keyCode, event)
        if(keyCode == cc.KeyCode.KEY_LEFT_ARROW) then
            isPushLeft = true
        elseif(keyCode == cc.KeyCode.KEY_RIGHT_ARROW) then
            isPushRight = true
        elseif(keyCode == cc.KeyCode.KEY_DOWN_ARROW) then
            -- sliding
            isPushDown = true
        elseif(keyCode == cc.KeyCode.KEY_Z) then
            -- buster
            isPushBuster = true 
        elseif(keyCode == cc.KeyCode.KEY_SPACE) then
            -- jump
            if(isPushBuster) then
                -- sliding
            end
        end
    end

    local function onKeyReleased(keyCode, event)
        if(keyCode == cc.KeyCode.KEY_LEFT_ARROW) then
            isPushLeft = false
        elseif(keyCode == cc.KeyCode.KEY_RIGHT_ARROW) then
            isPushRight = false
        elseif(keyCode == cc.KeyCode.KEY_Z) then
            -- buster
            isPushBuster = false
        elseif(keyCode == cc.KeyCode.KEY_SPACE) then
            -- jump
        end
    end

    local listener = cc.EventListenerKeyboard:create()
    listener:registerScriptHandler(onKeyPressed,cc.Handler.EVENT_KEYBOARD_PRESSED)
    listener:registerScriptHandler(onKeyReleased,cc.Handler.EVENT_KEYBOARD_RELEASED)

    local eventDispatcher = layerBackground:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, layerBackground)

    local function onNodeEvent(event)
        if "exit" == event then
            cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.schedulerID)
        end
    end
    layerBackground:registerScriptHandler(onNodeEvent)

    return layerBackground
end

-- create menu
function GameScene:createLayerMenu()

    local layerMenu = cc.Layer:create()
    local menuPopup, menuTools, effectID

    -- 閉じる
    local function menuCallbackClosePopup()
        -- stop test sound effect
        cc.SimpleAudioEngine:getInstance():stopEffect(effectID)
        menuPopup:setVisible(false)
    end

    -- メニューのポップアップ
    local function menuCallbackOpenPopup()
        -- うるさい音
        --local effectPath = cc.FileUtils:getInstance():fullPathForFilename("effect1.wav")
        --effectID = cc.SimpleAudioEngine:getInstance():playEffect(effectPath)
        menuPopup:setVisible(true)
    end

    -- add a popup menu
    -- メニュー
    local menuPopupItem = cc.MenuItemImage:create("menu2.png", "menu2.png")
    menuPopupItem:setPosition(0, 0)
    -- 閉じるコールバック登録
    menuPopupItem:registerScriptTapHandler(menuCallbackClosePopup)
    menuPopup = cc.Menu:create(menuPopupItem)
    menuPopup:setPosition(self.origin.x + self.visibleSize.width / 2, self.origin.y + self.visibleSize.height / 2)
    menuPopup:setVisible(false)
    layerMenu:addChild(menuPopup)

    -- add the left-bottom "tools" menu to invoke menuPopup
    -- 除草ボタン
    local menuToolsItem = cc.MenuItemImage:create("menu1.png", "menu1.png")
    menuToolsItem:setPosition(0, 0)
    -- 開くコールバック登録
    menuToolsItem:registerScriptTapHandler(menuCallbackOpenPopup)
    menuTools = cc.Menu:create(menuToolsItem)
    local itemWidth = menuToolsItem:getContentSize().width
    local itemHeight = menuToolsItem:getContentSize().height
    menuTools:setPosition(self.origin.x + itemWidth/2, self.origin.y + itemHeight/2)
    layerMenu:addChild(menuTools)

    return layerMenu
end

return GameScene
